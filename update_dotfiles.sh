#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
HOME_DIR=~
CONFIG_DIR="$HOME_DIR/.config"

dot_config_configs=(
	bspwm
	sxhkd
	polybar
	nvim
	kitty
	dunst
	rofi
)

home_configs=(
	.vim
	.vimrc
	.zshrc
	.tmux.conf
	.tmux
)

copy_dot_config_dir() {
	echo Copying $CONFIG_DIR/$1 to $SCRIPT_DIR/.config
	cp -r $CONFIG_DIR/$1 $SCRIPT_DIR/.config
}

copy_all_dot_configs() {
	for i in "${dot_config_configs[@]}"
	do
		copy_dot_config_dir "$i"
	done
}

copy_home_config() {
	echo Copying $HOME_DIR/$1 to $SCRIPT_DIR
	cp -r $HOME_DIR/$1 $SCRIPT_DIR
}

copy_all_home_configs() {
	for i in "${home_configs[@]}"
	do
		copy_home_config "$i"
	done
}

echo "--- .config CONFIGS ---"
copy_all_dot_configs
echo "--- home (~) CONFIGS ---"
copy_all_home_configs

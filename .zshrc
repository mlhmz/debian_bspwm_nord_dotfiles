# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt extendedglob nomatch notify
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/malek/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall



export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# Load Angular CLI autocompletion.
source <(ng completion script)

# User Config
alias ls='ls --color=auto'

# Using PS1, its loading the config once, while PROMPT is loading it to every prompt
PS1="%n@thinkpad %F{blue}%/%f $ "

[[ -s "/home/malek/.gvm/scripts/gvm" ]] && source "/home/malek/.gvm/scripts/gvm"
